couru
=====

NOTE: At the moment you need Rust nightly-2016-05-07. With multirust, you need to run multirust override nightly-2016-05-07 in the generated project directory for things to work as expected.

Build
-----

    $ rebar3 compile

```
1> Counter = couru:new().
2> couru:read(Counter).
0
3> couru:incr(Counter).
4> couru:read(Counter).
1
```
