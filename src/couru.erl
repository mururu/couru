-module(couru).
-export([new/0, incr/1, incr/2, read/1]).
-on_load(init/0).

new() ->
    not_loaded(?LINE).

incr(_) ->
    not_loaded(?LINE).

incr(_, _) ->
    not_loaded(?LINE).

read(_) ->
    not_loaded(?LINE).

init() ->
    SoName = case code:priv_dir(couru) of
        {error, bad_name} ->
            case filelib:is_dir(filename:join(["..", priv])) of
                true ->
                    filename:join(["..", priv, libcouru]);
                _ ->
                    filename:join([priv, libcouru])
            end;
        Dir ->
            filename:join(Dir, libcouru)
    end,
    erlang:load_nif(SoName, 0).

not_loaded(Line) ->
    exit({not_loaded, [{module, ?MODULE}, {line, Line}]}).
