#![feature(plugin)]
#![plugin(rustler_codegen)]

#[macro_use]
extern crate rustler;
use rustler::{ NifEnv, NifTerm, NifResult, NifEncoder, NifError, atom };
use rustler::resource::ResourceCell;

use std::sync::atomic::{AtomicUsize, Ordering};

#[NifResource] 
struct Counter {
    counter: AtomicUsize
}

rustler_export_nifs!(
    "couru",
    [("new", 0, new),
     ("incr", 1, incr1),
     ("incr", 2, incr2),
     ("read", 1, read)],
    Some(on_load)
);

fn on_load(env: &NifEnv, _load_info: NifTerm) -> bool {
    resource_struct_init!(Counter, env);
    true
}

fn new<'a>(env: &'a NifEnv, _args: &Vec<NifTerm>) -> NifResult<NifTerm<'a>> {
    let counter = ResourceCell::new(Counter {
        counter: AtomicUsize::new(0)
    });
    Ok(counter.encode(env))
}

fn incr1<'a>(env: &'a NifEnv, args: &Vec<NifTerm>) -> NifResult<NifTerm<'a>> {
    incr(env, args, 1)
}

fn incr2<'a>(env: &'a NifEnv, args: &Vec<NifTerm>) -> NifResult<NifTerm<'a>> {
    let val: i64 = try!(args[1].decode());
    if val < 0 {
        Ok(NifError::BadArg.encode(env)) 
    } else {
        incr(env, args, Some(val as usize).unwrap())
    }
}

fn incr<'a>(env: &'a NifEnv, args: &Vec<NifTerm>, val: usize) -> NifResult<NifTerm<'a>> {
    let counter: ResourceCell<Counter> = try!(args[0].decode());
    // TODO
    counter.read().unwrap().counter.fetch_add(val, Ordering::Relaxed);
    Ok(atom::get_atom_init("ok").to_term(env))
}

fn read<'a>(env: &'a NifEnv, args: &Vec<NifTerm>) -> NifResult<NifTerm<'a>> {
    let counter: ResourceCell<Counter> = try!(args[0].decode());
    // TODO
    let count = Some(counter.read().unwrap().counter.load(Ordering::Relaxed) as u64);
    Ok(count.unwrap().encode(env))
}
